package Server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class ThreadPoolServer extends BaseServer<String> {
    
    private final ExecutorService pool;
 
    public ThreadPoolServer(
            int numThreads,
            int port,
            Supplier<MessagingProtocol<String>> protocolFactory,
            Supplier<MessageEncoderDecoder<String>> encoderDecoderFactory) {
 
        super(port, protocolFactory, encoderDecoderFactory);
        this.pool = Executors.newFixedThreadPool(numThreads);
    }
 
    @Override
    public void serve() {
        super.serve();
        pool.shutdown();
    }
 
    @Override
    protected void execute(ConnectionHandler<String> handler) {
        pool.execute(handler);
    }
}
