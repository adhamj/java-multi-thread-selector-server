package Server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ConnectionHandler<T> implements Runnable {
    
    private final MessagingProtocol<T> protocol;
    private final MessageEncoderDecoder<T> encdec;
    private final Socket sock;
 
    public ConnectionHandler(Socket sock, MessageEncoderDecoder<T> reader, MessagingProtocol<T> protocol) {
        this.sock = sock;
        this.encdec = reader;
        this.protocol = protocol;
    }
 
    @Override
    public void run() {
        
        try (   Socket sock = this.sock; //just for automatic closing
                BufferedInputStream in = new BufferedInputStream(sock.getInputStream());
                BufferedOutputStream out = new BufferedOutputStream(sock.getOutputStream())) {
 
            int read;
            while (!protocol.shouldTerminate() && (read = in.read()) >= 0) {
            	System.out.println("client" + sock);
                T nextMessage = encdec.decodeNextByte((byte) read);
                
                if (nextMessage != null) {
                	
                    T response = protocol.process(nextMessage);
                    if (response != null) {
                    	
                        out.write(encdec.encode(response));
                        out.flush();
                    }
                    
                }
            }
 
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
